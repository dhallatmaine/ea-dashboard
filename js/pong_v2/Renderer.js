var Renderer = {

  drawBall : function() {
    ctx.beginPath();
    ctx.arc(Ball.x, Ball.y, Ball.RADIUS, 0, 2 * Math.PI, false);
    ctx.fill();
  },

  drawPaddle : function() {
    ctx.fillRect(Paddle.x, Paddle.y, Paddle.WIDTH, Paddle.HEIGHT);
  },

  drawEnemyPaddle : function() {
    ctx.fillRect(EnemyPaddle.x, EnemyPaddle.y, EnemyPaddle.WIDTH, EnemyPaddle.HEIGHT);
  },

  clearCanvas: function() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  },

  clearAndDrawPieces: function() {
    Renderer.clearCanvas();
    Renderer.drawPaddle();
    Renderer.drawEnemyPaddle();
    Renderer.drawBall();
  }

};