var Paddle = {

  HEIGHT: 10,
  WIDTH: 100,
  x: 0,
  y: canvas.height - 10,

  move : function(event) {
    if (event.pageX >= CANVAS_LEFT && event.pageX <= CANVAS_RIGHT) {
      Paddle.x = event.pageX - CANVAS_LEFT;
    } else if (event.pageX < CANVAS_LEFT) {
      Paddle.x = 0;
    } else if (event.pageX > CANVAS_RIGHT) {
      Paddle.x = canvas.width - Paddle.WIDTH;
    }
  }

};