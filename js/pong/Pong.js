var Pong = {

  SPEED_INCREMENT: 0.2,
  score: 0,

  determineBallTravelDirection: function() {
    if (Ball.x - (Ball.RADIUS / 2) <= 0) {
      Ball.x_direction = Direction.EAST;
    } else if (Ball.x + (Ball.RADIUS / 2) >= canvas.width){
      Ball.x_direction = Direction.WEST;
    }

    if (Ball.y - (Ball.RADIUS / 2) <= 0) {
      Ball.y_direction = Direction.SOUTH;
    } else if (Ball.y + (Ball.RADIUS / 2) >= canvas.height - Paddle.HEIGHT) {
      if (Pong.ballCollidesWithPaddle()) {
        Ball.y_direction = Direction.NORTH;
        Ball.speed += Pong.SPEED_INCREMENT;
        Pong.increaseScore();
      } else {
        Pong.lose();
      }
    }
  },

  lose : function() {
    Pong.resetDefaults();
    Pong.play();
  },

  increaseScore : function() {
    Pong.score += 1;
    Pong.updateScore();
  },

  updateScore : function() {
    document.getElementById("score-holder").innerHTML = "" + Pong.score;
  },

  ballCollidesWithPaddle: function() {
    /* make sure ball is within the left and right edge of the paddle */
    if ( (Ball.x >= Paddle.x && Ball.x <= Paddle.x + Paddle.WIDTH) &&
        Ball.y === (canvas.height - Paddle.HEIGHT)) {
      return true;
    }
    return false;
  },

  play : function() {
    Renderer.clearAndDrawPieces();
    Pong.determineBallTravelDirection();
    Ball.move();
  },

  resetDefaults : function() {
    Pong.score = 0;
    Pong.updateScore();

    Paddle.x = (canvas.width / 2) - (Paddle.WIDTH / 2);
    Paddle.y = canvas.height - Paddle.HEIGHT;

    Ball.x = canvas_center_x;
    Ball.y = canvas_center_y;
    Ball.x_direction = Math.round(Math.random());
    Ball.y_direction = Direction.NORTH;
    Ball.speed = 5;
  }

};