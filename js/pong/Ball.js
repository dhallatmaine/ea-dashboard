var Ball = {

  RADIUS: 6,

  x: canvas_center_x,
  y: canvas_center_y,
  x_direction: Math.round(Math.random()),
  y_direction: Direction.NORTH,

  speed: 5,

  move : function() {
    if (Ball.x_direction === Direction.EAST) {
      Ball.moveRight();
    } else {
      Ball.moveLeft();
    }

    if (Ball.y_direction === Direction.NORTH) {
      Ball.moveUp();
    } else {
      Ball.moveDown();
    }
  },

  moveRight : function() {
    Ball.x += Ball.speed;
  },

  moveLeft : function() {
    Ball.x -= Ball.speed;
  },

  moveUp : function() {
    Ball.y -= Ball.speed;
  },

  moveDown : function() {
    Ball.y += Ball.speed;
  }

};